# SQL 语法

在不同的数据库管理系统中，实例和模式都有不同的概念。在 MySQL 中，分别有如下意义：

- 实例（instance）：一个操作系统中启动中的一个mysqld守护进程被称为一个数据库实例。
- 数据库（database）：存放多个相关业务数据表的逻辑对象。
- 模式（schema）：MySQL中schema等同于database。


## SQL语法和注释

```SQL
-- MySQL有几种注释语法

-- 用双斜线注释一行

SELECT 1+1;     -- 注释文字


-- 下面这段注释表示：当接收SQL的Server版本大于4.0.001时，后面的注释文字就可以被识别。
SELECT /*!40001 SQL_NO_CACHE */  count(*) FROM db_test.test_table
SELECT SQL_NO_CACHE  count(*) FROM db_test.test_table


```


## 数据库的创建与删除

```sql
-- 建库，在Unix中，库名是严格区分大小的，强烈建议库名小写
CREATE DATABASE `database_test`  IF NOT EXISTS CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_0900_ai_ci';

-- 设置某个库只读
-- MySQL 8.0.22中引入的READ ONLY选项控制是否允许修改数据库和其中的对象。允许的值为DEFAULT或0（非只读）和1（只读）。
-- 此选项对于数据库迁移非常有用，因为启用了只读功能的数据库可以迁移到另一个MySQL实例，而不必担心在操作过程中数据库可能会发生更改。

ALTER DATABASE  `database_test`   READ ONLY = 0 ;


-- 删库
DROP DATABASE database_test;


-- 修改数据库
-- 注意：在 MySQL 中，数据库创建之后库名是无法直接修改的。能改的只有字符集等属性。

```



### 表的创建

```SQL

-- 文件大小写，在不同的操作系统中的文件系统，是不一样的。
-- Linux操作系统，严格区分大小写，Test 和 TEST 是两个不同的文件或文件夹。
-- Windows操作系统，不区分大小写，无法同时创建 Test 和 TEST 两个同名的文件或文件夹。

-- 对于MySQL数据库和表，要在操作系统创建与库名和表名相同的文件夹或文件。

-- lower_case_file_system 是一个只读参数，无法被修改，这个参数是用来告诉你在当前的系统平台下，是否对文件名大小写敏感。

-- lower_case_table_names是一个MySQL可配置的参数，它的取值如下：

-- 0 大小写敏感。（Unix，Linux默认） 创建的库表将原样保存在磁盘上。
-- 如 create database TeSt ; 将会创建一个TeSt的目录，create table AbCCC …将会原样生成AbCCC.frm。 SQL语句也会原样解析。

-- 1 大小写不敏感。（Windows默认） 创建库表时，MySQL将所有的库表文件转换成小写存储在磁盘上。SQL语句同样会将库表名转换成小写。 
-- 如需要查询以前创建的Testtable（生成Testtable.frm文件），即便执行select * from Testtable，也会被转换成select * from testtable，致使报错表不存在。


-- 最佳实践：建议将lower_case_table_names值统一设置为0，在代码中对大小写进行严格区分
-- 库名，表名，字段名，统一由小写英文字符和数字组成。




-- 建表语法

-- 表名，字段名，字段类型，字段的约束，字段的字符集，存储引擎等
CREATE TABLE `cwd_group` (
  `id` bigint NOT NULL,
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `lower_group_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` char(1) COLLATE utf8_bin NOT NULL,
  `local` char(1) COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `group_type` varchar(32) COLLATE utf8_bin NOT NULL,
  `directory_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cwd_group_name_dir_id` (`lower_group_name`,`directory_id`),
  KEY `idx_group_active` (`active`,`directory_id`),
  KEY `idx_group_dir_id` (`directory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE Product (
    product_id        char(4)         NOT NULL,     
    product_name      varchar(100)    NOT NULL,     
    product_type      varchar(32)     NOT NULL,     
    sale_price        integer                 ,
    purchase_price    integer                 ,
    regist_date       date                    , 
    PRIMARY KEY (`product_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



```



### 查询基础

从表中选取筛选数据，需要使用 SELECT 语句，也就是用表中选出 SELECT 必要数据的意思。通过 SELECT 语句查询并选取必要数据的过程称为查询（query）

```SQL
-- 查询语句的基本结构

select A1,A2,A3 ... An
from r1,r2,r3 ... rn
where P

-- 注意：这个方法只是用来理解SQL语句的查询结果
-- from 多个关系后，可以理解为所有关系的笛卡尔积
-- 谓词P对关系进行过滤
-- 再从 select 中提取需要的列
-- 实际上，组成笛卡尔积的时候，会尽可能先执行谓词过滤一些数据后，再进行笛卡尔积
```


### 查询排序

```SQL

-- 对字符串类型中的中文字段排序

-- 



```


### 派生表和子查询

在数据库中，我们经常使用子查询和派生表来进行查询。

```SQL
-- 子查询
-- 在另一个查询(外部查询)中嵌套另一个查询语句(内部查询)，并使用内部查询的结果值作为外部查询条件。
-- 子查询在where中
-- where条件比对的值是从其他表查出来的。

SELECT 
       customerNumber, checkNumber, amount
FROM
   　　 payments
WHERE
 　　   amount = (SELECT  MAX(amount) FROMpayments);


-- 标量子查询 （一行一列）
-- 那些只返回一个单一值的子查询称之为标量子查询：子查询里面的查询结果只返回一行一列一个值的情况。
SELECT (SELECT m1 FROM e1 LIMIT 1);
SELECT * FROM e1 WHERE m1 = (SELECT MIN(m2) FROM e2);
SELECT * FROM e1 WHERE m1 < (SELECT MIN(m2) FROM e2);


-- 行子查询（一行多列）
-- 顾名思义，就是返回一条记录的子查询，不过这条记录需要包含多个列（只包含一个列就成了标量子查询了）。
-- 其中的(SELECT m2, n2 FROM e2 LIMIT 1)就是一个行子查询
-- 整条语句的含义就是要从 e1 表中找一些记录，这些记录的 m1 和 n1 列分别等于子查询结果中的 m2 和 n2 列
SELECT * FROM e1 WHERE (m1, n1) = (SELECT m2, n2 FROM e2 LIMIT 1);

-- 列子查询（一列数据）
-- 列子查询自然就是查询出一个列的数据，不过这个列的数据需要包含多条记录
-- 其中的(SELECT m2 FROM e2)就是一个列子查询，表明查询出 e2 表的 m2 列 的所有值作为外层查询 IN 语句的参数。
SELECT * FROM e1 WHERE m1 IN (SELECT m2 FROM e2);

-- 表子查询（二维多行多列）
-- 顾名思义，就是子查询的结果既包含很多条记录，又包含很多个列
-- 其中的(SELECT m2, n2 FROM e2)就是一个表子查询、此sql必须要在m1，n1都满足的条件下方可成立
SELECT * FROM e1 WHERE (m1, n1) IN (SELECT m2, n2 FROM e2);


-- From子句中的子查询
-- 派生表（子查询）
-- from后面跟的表是通过其他查询查出来的，这种查询叫派生表，派生表必须要有别名，以便稍后在查询中引用其名称。
-- 这种子查询后边的 AS t 表明这个子 查询的结果就相当于一个名称为 t 的表，这个名叫 t 的表的列就是子查询结果中的列（m和n）。
-- 这个放在 FROM 子句中的子查询本质上相当于一个表，但又和我们平常使用的表有点儿不一样，MySQL 把这种由子查询结果集组成的临时表称之为派生表。
SELECT m, n FROM (SELECT m2 + 1 AS m, n2 AS n FROM e2 WHERE m2 > 2) AS t;
-- 派生表也是从 select 语句中返回的虚拟表。
SELECT 
    column_list
FROM
    (SELECT column_list ... FROM table_1) derived_table_name
WHERE derived_table_name.c1 > 0;


```



### 联表查询



#### 笛卡尔积/交叉连接

笛卡尔积是指在数学中，两个集合X和Y的笛卡尓积（Cartesian product），又称直积，表示为X × Y，第一个对象是X的成员而第二个对象是Y的所有可能有序对的其中一个成员。

笛卡尔积又叫笛卡尔乘积，是一个叫笛卡尔的人提出来的。 简单的说就是两个集合相乘的结果。

假设集合A={a, b}，集合B={0, 1, 2}，则两个集合的笛卡尔积为{(a, 0), (a, 1), (a, 2), (b, 0), (b, 1), (b, 2)}。

遍历左表的每一行数据，用左表每一行数据分别于与右表的每一行数据做关联

```SQL
 select * from stu cross join class;
 select * from stu,class;
 select * from stu as a ,  class as b join on 1=1 ;
 -- 可以看出，笛卡尔乘积的运算量超级大，一般不会使用笛卡尔乘积做表的关联查询
```

!!! 注意
    在 MySQL 中， CROSS JOIN  等价于  INNER JOIN ， 这两个可以互换使用。但是在标准SQL中，这两个并不一样。




#### 自然连接

对于两个表，自然连接是先找出两个表所有共用的属性，然后在共用属性上做匹配，找出相同的行进行连接。（一定要注意连接谓词是所有的共有属性集合）


```SQL
-- 标准SQL写法一
select user_name,dept_name from user natural join dept ;                  --这个限制比较大，要求字段名称一致。
-- 标准SQL写法二
select user_name,dept_name from  user, dept where user.id = dept.id  ...  --所有共有属性都连接起来。
-- MySQL写法三

-- select name , id , title from A1 natural join A2 natural join A3
-- select name , id , title from A1 natural join A2 , A3 where A2.id = A3.id 
-- 这两个写法的结果可能不一样，
-- 第一个可以认为将A1 A2进行自然连接的结果，再与A3进行自然连接
-- 第二个可以认为将A1 A2进行自然连接的结果，再与A3进行等值连接 

```


#### 等值连接

对两个表，等值连接是明确一组属性上进行匹配。然后进行连接。(连接谓词是在选定的属性集合)，所以自然连接是一种特殊的等值连接。这种方法可能更加通用。

```SQL
-- 在这个例子中，只在ID上进行匹配。要求两个表都要有ID这个字段。
-- 使用 using 语法 ， select ... from T1 join T2 using(id)
select user_name,dept_name from user  join dept using (id) ;
-- 使用 join on 关键字，on条件允许在参与连接的关系上设置连接谓词。
select  user_name,dept_name from user join dept on user.id = dept.id ;
```

#### 内连接

内连接包括自然连接，不等值连接和等值连接。


##### 自然连接

对于两个表，自然连接是先找出两个表所有共用的属性，然后在共用属性上做匹配，找出这些值相等的行进行连接。（一定要注意连接谓词是所有的共有属性集合）并且结果会去重。

```SQL
-- 标准SQL写法一
select user_name,dept_name from user natural join dept ;                  --这个限制比较大，要求关联字段名称一致。
-- 标准SQL写法二
select user_name,dept_name from  user, dept where user.id = dept.id  ...  --所有共有属性都连接起来。

-- select name , id , title from A1 natural join A2 natural join A3
-- select name , id , title from A1 natural join A2 , A3 where A2.id = A3.id 
-- 这两个写法的结果可能不一样: 
-- 第一个可以认为将A1 A2进行自然连接的结果，再与A3进行自然连接
-- 第二个可以认为将A1 A2进行自然连接的结果，再与A3进行等值连接 

```

##### 等值连接

在连接条件中使用等于号(=)运算符比较被连接列的列值，其查询结果中列出被连接表中的所有列，包括其中的重复列。

```SQL
-- tb1 inner join tb1 on something
-- 不保留未匹配的元组
select  user_name,dept_name from students  left outer join dept on user.id = dept.id ;

```




##### 不等值连接

不等值连接：在连接条件使用除等于运算符以外的其它比较运算符比较被连接的列的列值。这些运算符包括>、>=、<=、<、!>、!<和<>。



### 分组查询

在标准SQL，正常的如果对一个表进行分组查询，分组会进行去重，每一组应该只有一行数据。

如果是需要一个组内有多行数据，需要组内排序，可以使用窗口函数。

**分组查询中的要查询的列必须是 group by 中要分组的列或者聚集函数。**

```SQL

-- 查询每个系的平均工资(系名，平均工资)
-- 先按系分组，再求每个系的平均工资
select dept_name ,avg(salary) from instructor group by dept_name

-- 单独使用 GROUP BY 关键字时，查询结果会只显示每个分组中的第一条记录
mysql> SELECT COUNT(sex) as num ,sex FROM tb_students_info GROUP BY sex;
+-------+------+
| num   | sex  |
+-------+------+
| 10    | 女   |
| 15    | 男   |
+-------+------+
2 rows in set (0.01 sec)
```

### 对分组限定条件

对分组的结果限定查询查询条件，使用 having 子句，为要分组的标准限定条件。

```SQL
--仅查询平均工资大于4000的系，列出这样系的系名和平均工资
select dept_name ,avg(salary) from instructor group by dept_name having avg(salary) > 4000;

select user_id  from user group by user_id 

-- 另外一种思路，把所有系的平均工资查出来，再从这个结果里面查平均工资大于4000的
select   dept_name , avg(salary) from ( select dept_name ,avg(salary)  from instructor group by dept_name ) where avg(salary) > 4000;
```


### 去重查询

distinct支持单列、多列的去重方式。

- 单列去重的方式简明易懂，即相同值只保留1个。
- 多列的去重则是根据指定的去重的列信息来进行，即只有所有指定的列信息都相同，才会被认为是重复的信息。

```SHELL
mysql> select * from talk_test;
+----+-------+--------+
| id | name  | mobile |
+----+-------+--------+
|  1 | xiao9 | 555555 |
|  2 | xiao6 | 666666 |
|  3 | xiao9 | 888888 |
|  4 | xiao9 | 555555 |
|  5 | xiao6 | 777777 |
+----+-------+--------+

进行单列去重后的结果：
mysql> select distinct(name) from talk_test;
+-------+
| name  |
+-------+
| xiao9 |
| xiao6 |
+-------+
2 rows in set (0.01 sec)

mysql> select distinct(mobile) from talk_test;
+--------+
| mobile |
+--------+
| 555555 |
| 666666 |
| 888888 |
| 777777 |
+--------+
**只会保留指定的列的信息

进行多列去重后的结果：
mysql> select distinct name,mobile from talk_test;
+-------+--------+
| name  | mobile |
+-------+--------+
| xiao9 | 555555 |
| xiao6 | 666666 |
| xiao9 | 888888 |
| xiao6 | 777777 |
+-------+--------+
**只有所有指定的列信息都相同，才会被认定为重复的信息
```


```SQL
-- 所有列都不同，才被认为是重复的。
SELECT DISTINCT * FROM TABLE 

-- 两条记录之间之后只有部分字段的值是有重复的，但是主键唯一
SELECT * FROM TABLE WHERE ID IN (SELECT MAX(ID) FROM TABLE GROUP BY [去除重复的字段名列表,....]) 


```



### 集合查询

```SQL
-- AS 更名的用处：给查询结果的列名改一个名字，给要查询的表改一个名字。尤其是在同一个表中进行比较。

-- 教师表  instructor(ID, name, dept_name, salary)
-- 找出满足下面条件的所有教师的姓名：他们的工资比 Biology 系教师的最低工资要高

-- 方法一
-- 将一个表和它自己进行笛卡尔积运算，然后再过滤
select distinct T.name from instructor as T, instructor as S where T.salary > S.salary and S.dep_tname='Biology';

-- >some 比较，至少比集合中某一个值要大
--方法二
--等价上面的写法，使用 > some 格式判断，子查询查出 Biology 系所有的工资组成集合。再用 >some 比较
select name from instructor where salary > some (select salary from instructor where dep_tname='Biology' );



--找出满足下面条件的所有教师的姓名：他们的工资比 Biology 系每个教师的最高工资要高（他们的工资比Biology中的每个教师的工资都要高）
-- >all 比较，比集合中的所有值都大。
select name from instructor where salary > all (select salary from instructor where dep_tname='Biology' );


-- 找出平均工资最高的系
-- 先查出所有系的平均工资做为一个集合，再在所有系的平均的工资里面比较，找出最大值。
select dept_name from instructor group by dept_name having avg(salary) >=all (select avg(salary) from instructor group by deptname); 

```


### SQL中的null和DEFAULT

**NULL**

在 SQL 中建表，每个字段后面都加上 NULL 或 NOT NULL 修饰符来指定该字段是否可以为空(NULL)，还是说必须填上数据(NOT NULL)。

MySQL默认情况下指定字段为NULL修饰符，如果一个字段指定为NOT NULL，MySQL则不允许向该字段插入空值(这里面说的空值都为NULL)，因为这是“规定”。

但是在自增列和TIMESTAMP字段中，这个规则并不适用。向这些字段中插入NULL值将会导致插入下一个自动增加的值或者当前的时间戳。

**NULL 不是一个「值」，而是「没有值」。**

在 MySQL 中 null 不能使用任何运算符与其他字段或者变量（函数、存储过程）进行运算。若使用运算数据就可能会有问题。


在写 SQL 条件语句时经常用到 不等于 != 的筛选条件。

此时要注意此条件会将字段为 Null 的数据也当做满足不等于的条件而将数据筛选掉。（也就是说会忽略过滤掉为 null 的数据，导致数据不准确）。







# 运算符



数据库，除了数据的存取之外，还支持各种运算，主要包括**算数运算**和**逻辑运算**。





| 算数运算符 | 作用 |
| :--------- | :--- |
| +          | 加法 |
| -          | 减法 |
| *          | 乘法 |
| / 或 DIV   | 除法 |
| % 或 MOD   | 取余 |



| 运算符      |      |      |
| ----------- | ---- | ---- |
| =           |      |      |
| <>, !=      |      |      |
| >           |      |      |
| <           |      |      |
| >=          |      |      |
| <=          |      |      |
| BETWEEN     |      |      |
| NOT BETWEEN |      |      |
| IN          |      |      |
| NOT IN      |      |      |
| <=>         |      |      |
| LIKE        |      |      |
|             |      |      |
|             |      |      |



| 逻辑运算符 |                |      |
| ---------- | -------------- | ---- |
| NOT 或！   | 表示逻辑非     |      |
| AND 或 &&  | 表示逻辑与运算 |      |
| OR 或 \|\| |                |      |
| <          |                |      |
| >=         |                |      |





对于MySQL三大数据类型，数值类型，字符，时间



当两个不同类型的数据进行运算时，为了使得它们能够兼容，MySQL 可能会执行隐式的数据类型转换。例如，MySQL 在需要时会自动将字符串转换为数字，反之亦然。



- 如果任意一个参数为 NULL，比较运算符的结果为 NULL，<=> 相等比较运算符除外。NULL <=> NULL 的运算结果为 true，不需要进行类型转换。
- 如果两个参数都是字符串，执行字符串比较。





# 函数



### 数学函数

```sql
ABS(x)         --返回x的绝对值
BIN(x)         --返回x的二进制（OCT返回八进制，HEX返回十六进制）
CEILING(x)     --返回大于x的最小整数值
EXP(x)         --返回值e（自然对数的底）的x次方
FLOOR(x)       --返回小于x的最大整数值
GREATEST(x1,x2,...,xn)
                --返回集合中最大的值
LEAST(x1,x2,...,xn)   
                --返回集合中最小的值
LN(x)           --返回x的自然对数
LOG(x,y)        --返回x的以y为底的对数
MOD(x,y)        --返回x/y的模（余数）
PI()            --返回pi的值（圆周率）
RAND()          --返回０到１内的随机值,可以通过提供一个参数(种子)使RAND()随机数生成器生成一个指定的值。
ROUND(x,y)      --返回参数x的四舍五入的有y位小数的值
SIGN(x)         --返回代表数字x的符号的值
SQRT(x)         --返回一个数的平方根
TRUNCATE(x,y)   --返回数字x截短为y位小数的结果

-- 建议数学函数，仅作用于数字类型的字段或常量上
```



### 聚合函数

### 字符串函数

```sql
ASCII(char)       --返回字符的ASCII码值
BIT_LENGTH(str)   --返回字符串的比特长度
CONCAT(s1,s2...,sn) 
                  --将s1,s2...,sn连接成字符串
CONCAT_WS(sep,s1,s2...,sn)
                  --将s1,s2...,sn连接成字符串，并用sep字符间隔
INSERT(str,x,y,instr) 
                  --将字符串str从第x位置开始，y个字符长的子串替换为字符串instr，返回结果
FIND_IN_SET(str,list)
                  --分析逗号分隔的list列表，如果发现str，返回str在list中的位置
LCASE(str)或LOWER(str) 
                  --返回将字符串str中所有字符改变为小写后的结果
LEFT(str,x)       --返回字符串str中最左边的x个字符
LENGTH(s)         --返回字符串str中的字符数
LTRIM(str)        --从字符串str中切掉开头的空格
POSITION(substr,str) 
                  --返回子串substr在字符串str中第一次出现的位置
QUOTE(str)        --用反斜杠转义str中的单引号
REPEAT(str,srchstr,rplcstr)
                  --返回字符串str重复x次的结果
REVERSE(str)      --返回颠倒字符串str的结果
RIGHT(str,x)      --返回字符串str中最右边的x个字符
RTRIM(str)        --返回字符串str尾部的空格
STRCMP(s1,s2)     --比较字符串s1和s2
TRIM(str)         --去除字符串首部和尾部的所有空格
UCASE(str)或UPPER(str) 
                  --返回将字符串str中所有字符转变为大写后的结果
```





**字符串函数**

```sql
-- 当前时间: 16:47:33
select curtime();



-- 当前日期+时间  2017-12-27 20:14:56 
select now();


-- sysdate() 日期时间函数跟 now() 类似。一般很少用到
-- 不同之处在于：now() 在执行开始时值就得到了， sysdate() 在函数执行时动态得到值。
select now(), sysdate(),sleep(3), now(),sysdate();
2019-12-29 23:49:27	2019-12-29 23:49:27	0	2019-12-29 23:49:27	2019-12-29 23:49:30

-- 当前MySQL版本  5.7.26-log
select version() 

-- 长度： 3 ，3 ，1   length返回字节长度，char_length返回字符数量
select length("我") , length("你") , char_length("你");

-- 日期格式化  2019-12-27 
select date_format(now(),'%y-%m-%d');



-- md5加密:8a6f60827608e7f1ae29d1abcecffc3a
select md5("andyqian");
-- 字符串拼接 andyqian
select concat("andy","qian");

-- if函数判断

select t.name,if(t.weight<80,'正常','肥胖') 体重 from t_customer t



# 当前
select DATE(CURRENT_TIME)
```



