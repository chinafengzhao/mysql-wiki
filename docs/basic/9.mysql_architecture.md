##  MySQL逻辑架构

![architecture](https://fengzhaonote.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F60e63f69-3261-48df-a6d9-34ac0f8d73a7%2FUntitled.png?table=block&id=d6d2f0cf-e51d-48c7-a078-9126b92ec080&spaceId=5468c791-49fa-43aa-825f-28501aabf76f&width=1920&userId=&cache=v2)

MySQL逻辑架构整体分为三层：

- 连接层：最上层为客户端层，并非MySQL所独有，诸如：连接处理、授权认证、安全等功能均在这一层处理。
- 中间层：MySQL大多数核心服务均在中间这一层，包括查询解析、分析、优化、缓存、内置函数(比如：时间、数学、加密等函数)。所有的跨存储引擎的功能也在这一层实现：存储过程、触发器、视图等。
- 存储引擎层：最下层为存储引擎，其负责MySQL中的数据存储和提取。和Linux中的文件系统类似，每种存储引擎都有其优势和劣势。中间的服务层通过API与存储引擎通信，这些API接口屏蔽了不同存储引擎间的差异。


MySQL Server是一个单进程多线程的服务程序，在 MySQL Server上 用 ps -ef | grep mysqld 就能看到其系统进程ID了。

MySQL请求处理流程

1. 客户端向MySQL服务器发送一条查询请求。
2. 服务器首先检查查询缓存，如果命中缓存，则立刻返回存储在缓存中的结果。否则进入下一阶段。
3. 服务器进行SQL解析、预处理、再由优化器生成对应的执行计划。
4. MySQL根据执行计划，调用存储引擎的API来执行查询。
5. 将结果返回给客户端，同时缓存查询结果。

每一个客户端发起一个新的请求都由服务器端的**连接/线程处理工具**负责接收客户端的请求并开辟一个新的内存空间，在服务器端的内存中生成一个新的线程。

当每一个用户连接到服务器端的时候就会在进程地址空间里生成一个新的线程用于响应客户端请求，用户发起的查询请求都在线程空间内运行，结果也在这里面缓存并返回给服务器端。

线程的重用和销毁都是由**连接/线程处理管理器**实现的。

综上所述：用户发起请求，连接/线程处理器开辟内存空间，开始提供查询的机制。

用户总是希望MySQL能够获得更高的查询性能，最好的办法是弄清楚MySQL是如何优化和执行查询的。

一旦理解了这一点，就会发现：很多的查询优化工作实际上就是遵循一些原则让MySQL的优化器能够按照预想的合理方式运行而已。



### **客户端/服务端通信协议**


一般来说， 不需要去理解 MySQL 通信协议的内部实现细节， 只需要大致理解通信协议是如何工作的。

MySQL客户端/服务端 **通信协议是“半双工”的：在任一时刻，要么是服务器向客户端发送数据，要么是客户端向服务器发送数据，这两个动作不能同时发生。**

这种协议让 MySQL 通信简单快速， 但是也从很多地方限制了 MySQL。一个明显的限制是， 这意味着没法进行流量控制。

一旦一端开始发送消息，另一端要接收完整个消息才能响应它，所以我们无法也无须将一个消息切成小块独立发送，也没有办法进行流量控制。



**客户端提交一个SQL请求给服务器时是直接用一个单独的数据包发送的，所以当查询语句很长的时候，比如一次性提交插入大量数据时，需要注意控制 `max_allowed_packet` 参数。**

```shell
-- max_allowed_packet表示服务器所能处理的请求包的最大值。默认是64MB，最大值是1GB
max_allowed_packet=64MB
```

但是需要注意的是，如果查询实在是太大，服务端会拒绝接收更多数据并抛出异常。


当服务器收到大于 max_allowed_packet 字节的信息包时，将发"信息包过大"错误，并关闭连接。对于某些客户端，如果通信信息包过大，在执行查询期间，可能会遇到"丢失与 MySQL 服务器的连接"错误。


与之相反的是，服务器响应给用户的数据通常会很多，由多个数据包组成。当服务器响应客户端请求时，客户端必须完整的接收整个返回结果，而不能简单的只取前面几条结果，然后让服务器停止发送。

这种情况下， 客户端若接收完整的结果， 然后取前面几条需要的结果， 或者接收完几条结果后就“粗暴” 地断开连接， 都不是好主意。

因而在实际开发中，**尽量保持查询简单且只返回必需的数据，减小通信间数据包的大小和数量是一个非常好的习惯**。

**这也是查询中尽量避免使用SELECT * 以及加上LIMIT限制的原因之一。** 

>> 引自《高性能 MySQL》。


当客户端从服务器取数据时， 看起来是一个拉数据的过程， 但实际上是 MySQL 在向客户端推送数据的过程。 客户端不断地接收从服务器推送的数据， 客户端也没法让服务器停下来。



InnoDB的数据是保存在主键索引上的，所以全表扫描实际上是直接扫描表的主键索引，对于单表超过系统内存的情况，查询会不会把数据都读到内存中导致系统内存耗尽呢？

**显然不会，实际上服务端并不需要保存一个完整的结果集**。取数据和发数据的流程是这样的：

- 获取一行，写到net_buffer中。这块内存的大小是由参数net_buffer_length定义的，默认是16k。

- 重复获取行，直到net_buffer写满，调用网络接口发出去。

- 如果发送成功，就清空net_buffer，然后继续取下一行，并写入net_buffer。

- 如果发送函数返回EAGAIN或WSAEWOULDBLOCK，就表示本地网络栈（socket send buffer）写满了，进入等待。直到网络栈重新可写，再继续发送。

一个查询在发送过程中，占用的MySQL内部的内存最大就是net_buffer_length这么大，并不会达到200G；

socket send buffer 也不可能达到200G（默认定义/proc/sys/net/core/wmem_default），如果**socket send buffer**被写满，就会暂停读数据的流程。

也就是说，MySQL 是**边读边发的**，这个概念很重要。这就意味着，如果客户端接收得慢，会导致MySQL服务端由于结果发不出去，这个事务的执行时间变长。



### **连接器**


连接器负责跟客户端建立连接、获取权限、维持和管理连接。

客户端使用命令行登陆时连接命令：`mysql -h$ip -P$port -u$user -p`

在完成TCP三次握手之后，连接器就要开始认证身份进行账密校验，校验通过之后，**连接器会到权限表里查询拥有的权限之后在这个连接里的权限判断，SQL执行的权限都依赖于此时读取的权限。**

这就意味着，一个用户成功建立连接后，即使你用管理员账号对这个用户的权限做了修改，也不会影响已经存在连接的权限。修改完成后，只**有再新建的连接才会使用新的权限设置**。

#### MySQL连接线程模型

MySQL是一个单进程多线程的软件，启动一个 MySQL 实例，操作系统中会使用 mysqld 这个可执行文件来启动一个mysqld进程。

mysqld 通过创建多个线程来服务于不同的用户连接。通常情况下，随着用户连接数的增加，MySQL内部用于处理用户连接的线程也会同步的增加，在一定范围内，增加用户并发连接，对提高系统的吞吐量有一定的帮助，然而用户并发连接数超过某个阈值，MySQL的性能反而会降低。

我们打开 htop 或 top 时，如果查看以线程方式查看，就可以看到很多 mysqld 线程。这些就是用于处理客户端连接创建的线程。




MySQL内部处理用户连接的方式严格来说，有三种：

- 单线程处理所有用户连接，一般在调试时使用。
- `one-thread-per-connection` ， 多线程处理用户连接，一个线程对应一个用户连接，也是 `MySQL Community Server` 默认的连接处理方式。
- `Thread pool`, 在Percona，MariaDB，Oracle MySQL 企业版，以及[阿里云polarDB](https://help.aliyun.com/document_detail/206413.htm)中提中，提供了**线程池**特性。

在 `one-thread-per-connection` 情况下，每个连接分配一个线程，当客户端和MySQL服务器建立TCP连接之后，MySQL服务器就会给这个连接分配一个线程，当这个连接收到SQL时，对应的线程就执行这个SQL，而当SQL执行结束后，这个线程就去Sleep，等待客户端的新请求。这个线程会一直存活，直到客户端退出登录或线程超时断开（由wait_timeout或interactive_timeout参数控制，），并关闭连接，这个线程才会退出（或者进入MySQL的ThreadCache）。

超时参数，详见 https://zhuanlan.zhihu.com/p/82554484


 - 短连接，应用程序和数据库通信完毕之后立即连接关闭连接。短连接的坏处：
   
    - **增加数据库服务器CPU上下文切换开销，影响数据库服务器性能。**

    - **多次反复TCP握手连接，效率低下。**

 - 长连接，应用程序和数据库通信处理完成之后，不主动关闭，并且 MySQL 连接超时退出时间很长。连接维持的时间相对较长。

    - **多长连接可能也是灾难。长期占用资源不释放**。


MySQL的最大连接数由 `max_connections` 参数控制，在5.7版本中默认是151， 最大可以达到16384（2^14）。设置的太小，连接满了之后后面的新连接就会报`too many connections`


MySQL Server其实默认允许的最大客户端连接数位max_connections + 1 ，这其中额外可以登陆的1仅仅允许拥有super权限的用户进行连接。

MySQL如此设计其实是为了当数据库出现连接数打满的情况下，可以使用同时拥有super、process权限的高权限数据库账号登陆数据库，将问题会话或者一些空闲连接进行kill，紧急处理故障。

**基于此，数据库账号权限一定要做好明确的规划，业务账号仅仅拥有对应业务数据库的读写权限、高权限数据库账号用于运维管理。**


MySQL 官网给出了一个最大连接数推荐计算方式，`Max_used_connections / max_connections * 100% ≈ 85%`



MySQL的 **状态变量** 显示MySQL服务实例的运行状态信息，这些状态信息是动态的，包括MySQL服务器连接的会话状态、变量信息等。默认情况下状态变量都是以大写字母开头。

```shell
show status;
show session status;
show global status;
```

| 状态变量                          | 含义                                                         |
| --------------------------------- | ------------------------------------------------------------ |
| Connections                       | 状态变量：MySQL服务从初始化开始成功建立连接的数量，该值不断累加 |
| Max_used_connections              | 状态变量：MySQL服务从启动开始，同一时刻并发连接的最大值，如果该值很大，则有可能系统并发较高，可以考虑调大max_connections |
| Connection_errors_max_connections | 状态变量：当MySQL的最大并发连接数超过设置的max_connections变量的值，被拒绝的次数会记录到这个状态值里 |
| Threads_connected                 | 状态变量： MySQL server当前打开的连接数                      |

### 控制参数

| 配置                 | 含义                                                         |
| -------------------- | ------------------------------------------------------------ |
| max_connections      | 配置参数：从MySQL server层面限制总的所有账号一起最大的可连接的数量，默认151，最大100000 |
| max_user_connections | 配置参数：代表允许单个用户的连接数最大值，即并发值。默认为0表示不限制 |
| wait_timeout         | 配置参数：即MySQL长连接(非交互式)的最大生命时长，默认是8小时，根据业务特点配置 |
| interactive_timeout  | 配置参数：即MySQL长连接长连接(交互式)的最大生命时长，默认是8小时，根据业务特点配置 |
| connect_timeout      | 配置参数：获取MySQL连接是多次握手的结果，除了用户名和密码的匹配校验外，还有IP->HOST->DNS->IP验证，任何一步都可能因为网络问题导致线程阻塞。为了防止线程浪费在不必要的校验等待上，超过connect_timeout的连接请求将会被拒绝。默认是10秒 |

#### 线程缓存

线程缓存实现在MySQL server端，client连接之后对应的sql线程在服务端会被缓存起来，缓存的线程数量由 [thread_cache_size](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_thread_cache_size) 大小决定。

当服务器不断有大量连接创建、关闭的场景下，使用线程缓存能够重用缓存起来的线程，避免了大量线程反复创建销毁带来的CPU上下文切换性能消耗，但是仍然无法解决高连接数带来的线程数过高的问题。

```shell
# 查看线程缓存大小
show global variables like 'thread_cache_size';

# 可以通过如下几个MySQL状态值来适当调整线程池的大小
show global status like 'Threads_%';
```

| 配置              | 含义                                                         |
| ----------------- | ------------------------------------------------------------ |
| Threads_cached    | 状态变量：当前线程池中缓存有多少空闲线程                     |
| Threads_connected | 状态变量：当前的连接数 ( 也就是线程数 )                      |
| Threads_created   | 状态变量：已经创建的线程总数                                 |
| Threads_running   | 状态变量：当前激活的线程数 ( Threads_connected 中的线程有些可能处于休眠状态 ) |



### 线程池

在线程池方案下，通常在MySQL server服务端实现，MySQL 通过预先创建一定数量的线程，在监听到有新的请求时，线程池直接从现有的线程中分配一个线程来提供服务，服务结束后这个线程不会直接销毁，而是又去处理其他的请求。

这样就避免了线程和内存对象频繁创建和销毁，减少了上下文切换，提高了资源利用率，从而在一定程度上提高了系统的性能和稳定性。

**线程池技术限制了并发线程数，相当于限制了MySQL的runing线程数，无论系统目前有多少连接或者请求，超过最大设置的线程数的都需要排队，让系统保持高性能水平，从而防止DB出现雪崩，对底层DB起到保护作用。**

### MySQL连接池

MySQL连接池，连接池通常实现在client端，是指应用(客户端)预先创建一定的连接，利用这些连接服务于客户端所有的DB请求。如果某一个时刻，空闲的连接数小于DB的请求数，则需要将请求排队，等待空闲连接处理。

通过连接池的连接复用，避免连接的频繁创建和释放，从而减少请求的平均响应时间，并且在请求繁忙时，通过请求排队，可以缓冲应用对DB的冲击。常见的MySQL连接池Tomcat、WildFly(JBoss)、 c3p0、 Druid等。

在很多公司，有不少程序员写代码，懒得用数据库连接池，所以就在每次数据操作时，临时连接数据库，使用完后直接关闭，这显然不好。 


[MySQL线程池与连接池](https://fengzhao.notion.site/MySQL-c5d5f4871cca4ea3ac434fa826095a1c)



### 线程处理和线程事务

```sql
-- 查看所有连接线程，其中ID为线程ID
select * from information_schema.`PROCESSLIST` 

-- 在MySQL中有两个kill命令：
-- 一个是kill query +线程id，表示终止这个线程中正在执行的语句；
-- 一个是kill connection +线程id，这里connection可缺省，表示断开这个线程的连接，当然如果这个线程有语句正在执行，也是要先停止正在执行的语句的。

-- 经常遇到使用了kill命令，却没能断开这个连接。再执行show processlist命令，看到这条语句的Command列显示的是Killed。

-- 查看事务执行情况，其中trx_id为事务ID，trx_mysql_thread_id为线程ID
select * from information_schema.INNODB_TRX

```





### **查询缓存**

MySQL 的查询，主要处理过程都是从硬盘中读取数据加载到内存，然后通过网络发给客户端，

MySQL 以前有一个查询缓存 Query Cache，从 MySQL8.0 开始，不再使用这个查询缓存，随着技术的进步，经过时间的考验，MySQL的工程团队发现启用缓存的好处并不多。所以在 8.0 中移除了这个特性。

在解析一个查询语句前，如果查询缓存是打开的，那么MySQL会检查这个查询语句是否命中查询缓存中的数据。如果当前查询恰好命中查询缓存，在检查一次用户权限后直接返回缓存中的结果。

这种情况下，查询不会被解析，也不会生成执行计划，更不会执行。

MySQL将缓存存放在一个引用表（不要理解成table，可以认为是类似于HashMap的数据结构），通过一个哈希值索引，这个哈希值通过查询本身、当前要查询的数据库、客户端协议版本号等一些可能影响结果的信息计算得来。

**所以两个查询在任何字符上的不同（例如：空格、注释），都会导致缓存不会命中。如果查询中包含任何用户自定义函数、存储函数、用户变量、临时表、mysql库中的系统表，其查询结果都不会被缓存。**

**比如函数 NOW() 或者 CURRENT_DATE() 会因为不同的查询时间，返回不同的查询结果。再比如包含 CURRENT_USER 或者 CONNECION_ID() 的查询语句会因为不同的用户而返回不同的结果，将这样的查询结果缓存起来没有任何的意义。**

既然是缓存，就会失效，那查询缓存何时失效呢？

MySQL的查询缓存系统会跟踪查询中涉及的每个表，如果这些表（数据或结构）发生变化，那么和这张表相关的所有缓存数据都将失效。

正因为如此，在任何的写操作时，MySQL必须将对应表的所有缓存都设置为失效。如果查询缓存非常大或者碎片很多，这个操作就可能带来很大的系统消耗，甚至导致系统僵死一会儿。

而且查询缓存对系统的额外消耗也不仅仅在写操作，读操作也不例外：

1. 任何的查询语句在开始之前都必须经过检查，即使这条SQL语句永远不会命中缓存
2. 如果查询结果可以被缓存，那么执行完成后，会将结果存入缓存，也会带来额外的系统消耗

首先，查询缓存的效果取决于缓存的命中率，只有命中缓存的查询效果才能有改善，因此无法预测其性能。

其次，查询缓存的另一个大问题是它受到单个互斥锁的保护。在具有多个内核的服务器上，大量查询会导致大量的互斥锁争用。

通过基准测试发现，大多数工作负载最好禁用查询缓存（5.6的默认设置）：query_cache_type = 0

如果你认为会从查询缓存中获得好处，请按照实际情况进行测试。

- 数据写的越多，好处越少
- 缓冲池中容纳的数据越多，好处越少
- 查询越复杂，扫描范围越大，则越受益

==**最后的忠告是不要轻易打开查询缓存，特别是写密集型应用。**==

如果你实在是忍不住，可以将query_cache_type设置为DEMAND，这时只有加入SQL_CACHE的查询才会走缓存，其他查询则不会，这样可以非常自由地控制哪些查询需要被缓存。

对于一些热点数据，现在比较流行的做法是引入外部的缓存中间件，比如 redis 等，这个以后展开再讲。

### **语法解析和预处理**

MySQL通过关键字将SQL语句进行解析，并生成一颗对应的解析树。这个过程解析器主要通过语法规则来验证和解析。

比如SQL中是否使用了错误的关键字或者关键字的顺序是否正确等等。预处理则会根据MySQL规则进一步检查解析树是否合法。比如检查要查询的数据表和数据列是否存在等等。

### **查询优化**

经过前面的步骤生成的语法树被认为是合法的了，并且由优化器将其转化成查询计划。

==多数情况下，一条查询可以有很多种执行方式，最后都返回相应的结果。优化器的作用就是找到这其中最好的执行计划。==

MySQL采用了基于开销成本的优化器，以确定处理查询的最解方式，也就是说执行查询之前，都会先选择一条自以为最优的方案，然后执行这个方案来获取结果。

在很多情况下，MySQL能够计算最佳的可能查询计划，但在某些情况下，MySQL没有关于数据的足够信息，或者是提供太多的相关数据信息，估测就不那么友好了。

**对于一些执行起来十分耗费性能的语句，MySQL 还是依据一些规则，竭尽全力的把这个很糟糕的语句转换成某种可以比较高效执行的形式，这个过程也可以被称作查询重写**。

**它尝试预测一个查询使用某种执行计划时的成本，并选择其中成本最小的一个。**

基于成本的优化器将枚举可能的执行计划，并为每个计划分配成本，成本是执行该计划所需的时间和资源的估计值。

一旦这些可能性被列举出来，优化器就会选择成本最低的计划并将其交付执行。虽然成本模型通常被设计为最大化吞吐量（即每秒查询），但它也可以被设计为支持其他期望指标的查询行为，例如最小化延迟（即检索第一行的时间）或最小化内存使用。


#### 优化器和查询成本

一般来说一个sql查询可以有不同的执行方案，可以选择走某个索引进行查询，也可以选择全表扫描。

**查询优化器**则会比较并选择其中成本最低的方案去执行查询。



查询成本分大体为两种：

- **I/O成本**：磁盘读写的开销。一个查询或一个写入，都要从磁盘中读写数据，要一定的IO开销。

- **CPU成本**：关联查询，条件查找，都要CPU来进行计算判断，一定的计算开销。

MySQL使用的InnoDB引擎会把数据和索引都存储到磁盘上，当查询的时候需要先把数据先加载到内存中在进行下一步操作，这个加载的时间就是I/O成本。

当数据被加载到内存中后，CPU会计算查询条件匹配，对数据排序等等操作，这一步所消耗的时间就是CPU成本。

**但是查询优化器并不会真正的去执行sql，只会去根据优化的结果去预估一个成本。**

==**InnoDB引擎规定读取一个页面花费的成本默认约是0.25，读取以及检测一条记录是否符合搜索条件的成本默认约是0.1。**==

为什么都是约呢，因为MySQL内部的计算成本比较复杂这里提取了两个主要的计算参数。

```sql
## MySQL server 层面的各种开销
mysql> select * from mysql.server_cost;
+------------------------------+------------+---------------------+---------+---------------+
| cost_name                    | cost_value | last_update         | comment | default_value |
+------------------------------+------------+---------------------+---------+---------------+
| disk_temptable_create_cost   |       NULL | 2021-09-24 14:47:20 | NULL    |            20 |
| disk_temptable_row_cost      |       NULL | 2021-09-24 14:47:20 | NULL    |           0.5 |
| key_compare_cost             |       NULL | 2021-09-24 14:47:20 | NULL    |          0.05 |
| memory_temptable_create_cost |       NULL | 2021-09-24 14:47:20 | NULL    |             1 |
| memory_temptable_row_cost    |       NULL | 2021-09-24 14:47:20 | NULL    |           0.1 |
| row_evaluate_cost            |       NULL | 2021-09-24 14:47:20 | NULL    |           0.1 |
+------------------------------+------------+---------------------+---------+---------------+
6 rows in set (0.00 sec)

mysql>

## MySQL 存储引擎层面的各种开销
mysql> select * from mysql.engine_cost;
+-------------+-------------+------------------------+------------+---------------------+---------+---------------+
| engine_name | device_type | cost_name              | cost_value | last_update         | comment | default_value |
+-------------+-------------+------------------------+------------+---------------------+---------+---------------+
| default     |           0 | io_block_read_cost     |       NULL | 2021-09-24 14:47:20 | NULL    |             1 |
| default     |           0 | memory_block_read_cost |       NULL | 2021-09-24 14:47:20 | NULL    |          0.25 |
+-------------+-------------+------------------------+------------+---------------------+---------+---------------+
2 rows in set (0.00 sec)

mysql>
```


在 MySQL 可以通过查询当前会话的 last_query_cost 的值来得到其计算当前查询的成本。

```sql
 mysql> select * from t_message limit 10;
 ...省略结果集
 
 mysql> show status like 'last_query_cost';
 +-----------------+-------------+
 | Variable_name   | Value       |
 +-----------------+-------------+
 | Last_query_cost | 6391.799000 |
 +-----------------+-------------+
 ————————————————
```

示例中的结果表示优化器认为大概需要做 6391 个数据页的随机查找才能完成上面的查询。

这个结果是根据一些列的统计信息计算得来的，这些统计信息包括：每张表或者索引的页面个数、索引的基数、索引和数据行的长度、索引的分布情况等等。

有非常多的原因会导致 MySQL 选择错误的执行计划，比如统计信息不准确、不会考虑不受其控制的操作成本（用户自定义函数、存储过程）。

**MySQL认为的最优跟我们想的不一样（我们希望执行时间尽可能短，但 MySQL 选择它认为成本小的，但成本小并不意味着执行时间短）等等。**



#### 统计数据

MySQL统计信息是指数据库通过采样、统计出来的表、索引的相关信息，例如，表的记录数、聚集索引page个数、字段的Cardinality....。

MySQL在生成执行计划时，需要根据索引的统计信息进行估算，计算出最低代价（或者说是最小开销）的执行计划，MySQL支持有限的索引统计信息，因存储引擎不同而统计信息收集的方式也不同。

根据统计数据是否可以持久化，MySQL提供了两种统计方式：

- **统计数据存储在磁盘上。** 

- 统计数据存储在内存中，当服务器关闭时这些这些统计数据就都被清除掉了。　　

MySQL给我们提供了系统变量innodb_stats_persistent来控制到底采用哪种方式去存储统计数据。

```shell
# 在MySQL 5.6.6之前，innodb_stats_persistent的值默认是OFF，InnoDB的统计数据默认是存储到内存的
innodb_stats_persistent=OFF
# 在MySQL 5.6.6之后，innodb_stats_persistent的值默认是ON，InnoDB的统计数据默认是存储到磁盘中
innodb_stats_persistent=ON
```



InnoDB默认是以表为单位来收集和存储统计数据的，也就是说我们可以把某些表的统计数据（以及该表的索引统计数据）存储在磁盘上，把另一些表的统计数据存储在内存中。

我们可以在创建和修改表的时候通过指定(STATS_PERSISTENT,STATS_AUTO_RECALC,STATS_SAMPLE_PAGES)属性来指明该表的统计数据存储方式，以及其他属性。

持久化的统计数据存储在mysql.innodb_index_stats和mysql.innodb_table_stats中：







#### **优化器的功能**

1. 不改变语义的情况下，重写sql。重写后的 sql 更简单，更方便制定执行计划。
2. 根据成本分析，制定执行计划。





####  查看优化器重写后的SQL

SQL语句在被服务器执行时，并不一定就会按照我们写的顺序执行，MySQL优化器会重写sql，如何才能看到sql优化器重写后的sql呢？这就要用到explain extended和show warnings了。


explain extended sql语句，然后show warnings查看。

explain extended会输出sql的执行计划，查询记录的方式(全表扫描、全索引扫描、索引范围扫描等)、是否用上索引

show warnings会看到优化器重写后的sql



### **条件化简**

我们编写的查询语句的 where 搜索条件本质上是一个表达式，这些表达式可能比较繁杂，或者不能高效的执行，MySQL 的查询优化器会为我们简化这些表达式。

**移除不必要的括号**

有时候表达式里有许多无用的括号，比如这样一条 sql 条件：

```sql
 ((a = 5 AND b = c) OR ((a > c) AND (c < 5)))
 
 # 优化器就会对其进行优化成下面这样
 
 (a = 5 and b = c) OR (a > c AND c < 5)
```

**常量传递**

```sql
 a = 5 AND b > a 就可以被转换为： a = 5 AND b > 5 
```



**子查询优化**

我们查询中的 select 列 from 表 中，有时候，列和表可能是我们其他查询中出来的。这种列和表是用 select 语句表现出来的就叫子查询。外层 select 就叫外层查询。

- SELECT 子句

```sql
 SELECT (SELECT m1 FROM e1 LIMIT 1);
```

- FROM 子句

```sql
 -- 子查询后边的 AS t 表明这个子 查询的结果就相当于一个名称为 t 的表，这个名叫 t 的表的列就是子查询结果中的列（m和n）。
 -- 这个放在 FROM 子句中的子查询本质上相当于一个表，但又和我们平常使用的表有点儿不一样，MySQL 把这种由子查询结果集组成的表称之为派生表。
 SELECT m, n FROM (SELECT m2 + 1 AS m, n2 AS n FROM e2 WHERE m2 > 2) AS t;
```

- WHERE 或 ON 子句

```sql
 -- 最常见的查询：整个查询语句的意思就是我们想找 e1 表中的某些记录，这 些记录的 m1 列的值能在 e2 表的 m2 列找到匹配的值。
 SELECT * FROM e1 WHERE m1 IN (SELECT m2 FROM e2);
```

**子查询分类**

- 标量子查询 （一行一列）

```sql
 -- 那些只返回一个单一值的子查询称之为标量子查询：子查询里面的查询结果只返回一行一列一个值的情况。
 SELECT (SELECT m1 FROM e1 LIMIT 1);
 SELECT * FROM e1 WHERE m1 = (SELECT MIN(m2) FROM e2);
 SELECT * FROM e1 WHERE m1 < (SELECT MIN(m2) FROM e2);
 
```

- 行子查询（一行多列）

```sql
 -- 顾名思义，就是返回一条记录的子查询，不过这条记录需要包含多个列（只包含一个列就成了标量子查询了）。
 -- 其中的(SELECT m2, n2 FROM e2 LIMIT 1)就是一个行子查询
 -- 整条语句的含义就是要从 e1 表中找一些记录，这些记录的 m1 和 n1 列分别等于子查询结果中的 m2 和 n2 列
 SELECT * FROM e1 WHERE (m1, n1) = (SELECT m2, n2 FROM e2 LIMIT 1);
```

- 列子查询

```sql
 -- 列子查询自然就是查询出一个列的数据，不过这个列的数据需要包含多条记录
 -- 其中的(SELECT m2 FROM e2)就是一个列子查询，表明查询出 e2 表的 m2 列 的所有值作为外层查询 IN 语句的参数。
 SELECT * FROM e1 WHERE m1 IN (SELECT m2 FROM e2);
```

- 表子查询（二维多行多列）

```sql
 -- 顾名思义，就是子查询的结果既包含很多条记录，又包含很多个列
 -- 其中的(SELECT m2, n2 FROM e2)就是一个表子查询、此sql必须要在m1，n1都满足的条件下方可成立
 SELECT * FROM e1 WHERE (m1, n1) IN (SELECT m2, n2 FROM e2);
```


- 不相关子查询

如果子查询可以单独运行出结果，而不依赖于外层查询的值，我们就可以把这个子查询称之为不相关子查询。我们前边介绍的那些子查询全部都可以看作不相关子查询。


- 相关子查询

如果子查询的执行需要依赖于外层查询的值，我们就可以把这个子查询称之为相关子查询。

### **子查询在 MySQL 中是怎么执行的**

常规想象思维中子查询的执行方式（非实际）

```sql
 -- 不相关子查询
 SELECT * FROM s1 WHERE order_note IN (SELECT order_note FROM s2 WHERE order_no = 'a');
 
 -- 我们想象中的可能是把子查询里面的这个结果集查出来放到内存中，然后做为外层查询的条件进行查询。可能会导致两个问题：
 -- 1.子查询的结果集太多，可能内存中都放不下。
 -- 2.对于外层查询来说，如果子查询的结果集太多，那就意味着 IN 子句中的参数特别多，由于order_note不是索引列，每个IN语句的条件都会全表扫描进行遍历。
 
 -- 结果集过多的处理方案
 -- IN子句中的结果集可能存在着大量的重复字段。这些字段对于获取最后的查询结果而言，都是浪费资源的无用功，因此，结果集过多的第一个处理方案，就是考虑去重。
 -- 如果结果集中确实过大，导致即使结果去重后，内存存放仍然有压力，因此转存到磁盘当中。
 
 -- order_note不是索引，你怎么滴还能让他不进行全表扫描不成？当然，直接加索引是不成的。但是我们可以通过物化表的方式对sql进行改造，由优化器再次判断是否使用全表扫描。
 -- 当IN的结果集过大时，我们会将IN子句升级为物化表。升级流程如下：
 
 -- 1.该临时表的列就是子查询结果集中的列。
 -- 2.写入临时表的记录会被去重，临时表也是个表，只要为表中记录的所有列建立主键或者唯一索引。
 
 -- 物化表是基于磁盘的么？不，这个表在不是特别大的时候是基于内存的。
 -- 一般情况下子查询结果集不会大的离谱，所以会为它建立基于内存的使用 Memory 存储引擎的临时表，而且会为该表建立哈希索引。
 -- 如果子查询的结果集非常大，超过了系统变量 tmp_table_size 或者 max_heap_table_size，临时表会转而使用基于磁盘的存储引擎来保存结果集中的记录，索引类型也对应转变为 B+树索引。
 -- 当我们把子查询进行物化之后，假设子查询物化表的名称为 materialized_table，该物化表存储的子查询结果集的列为 m_val，那么这个查询就相当于表 s1 和子查询物化表 materialized_table 进行内连接。
 SELECT s1.* FROM s1 INNER JOIN materialized_table ON order_note = m_val
 
 
 
 
 -- 驱动表算法
 
 
```

[https://blog.csdn.net/weixin_47184173/article/details/117411011](https://blog.csdn.net/weixin_47184173/article/details/117411011)

[为什么300的并发能把支持最大连接数4000数据库压死？](https://www.notion.so/300-4000-5eb680aec38d4cc8adda2f70d33af246)